package com.acme.finishedapi;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Scanner;

import com.acme.finishedapi.resources.ProductResources;

import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.StaticHandler;

/**
 *
 *
 */
public class FinishedAPIVerticle extends AbstractVerticle {
	private static final Logger LOGGER = LoggerFactory.getLogger(FinishedAPIVerticle.class);
	
	public static void main(String[] args) {
		VertxOptions vertxOptions = new VertxOptions();
		vertxOptions.setClustered(true);
		
		Vertx.clusteredVertx(vertxOptions, resultHandler -> {
			if (resultHandler.succeeded()) {
				Vertx vertx = resultHandler.result();
				
				// Read config.json
				ConfigRetriever configRetriever = ConfigRetriever.create(vertx);
				configRetriever.getConfig(config -> {
					if (config.succeeded()) {
						JsonObject configJson = config.result();						
						DeploymentOptions options = new DeploymentOptions().setConfig(configJson);
						
						vertx.deployVerticle(new FinishedAPIVerticle(), options);
					}
				});
			}
		});
	}
	
	@Override
	public void start() throws Exception {
		LOGGER.info("Verticle FinishedAPIVerticle Started");
		
		Router router = Router.router(vertx);
		router.route().handler(CookieHandler.create());
		
		ProductResources productResources = new ProductResources();
		
		// Map subrouter for Products
		router.mountSubRouter("/api/", productResources.getAPISubRouter(vertx));
		
		router.get("/yo.html").handler(routingContext -> {
			Cookie nameCookie = routingContext.getCookie("name");
			
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("webroot/yo.html").getFile());
			
			String mappedHtml = "";
			
			try (Scanner scanner = new Scanner(file)) {
				StringBuilder result = new StringBuilder("");
								
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					result.append(line).append("\n");
				}
				
				mappedHtml = result.toString();
				
				String name = "unknow";
				if (nameCookie != null) {
					name = URLDecoder.decode(nameCookie.getValue(), "utf-8");
				} else {
					nameCookie = Cookie.cookie("name", URLEncoder.encode("Jose Luis", "utf-8"));
					nameCookie.setPath("/");
					nameCookie.setMaxAge(365 * 24 * 60 * 60);
					
					routingContext.addCookie(nameCookie);
					LOGGER.info("Add the name Cookie");
				}
				
				mappedHtml = mappedHtml.replace("{name}", name);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			routingContext.response().putHeader("Content-Type", "text/html").end(mappedHtml, "utf-8");
		});
		
		router.route().handler(StaticHandler.create().setCachingEnabled(false));

		vertx.createHttpServer().requestHandler(router::accept)
				.listen(config().getInteger("http.port"), asyncResult -> {
					if (asyncResult.succeeded()) {
						LOGGER.info("HTTP Server running on port "
								+ config().getInteger("http.port"));
					} else {
						LOGGER.error("Could not start a HTTP Server", asyncResult.cause());
					}
				});
	}
	
	@Override
	public void stop() throws Exception {
		LOGGER.info("Verticle FinishedAPIVerticle Stopped");		
	}
}
