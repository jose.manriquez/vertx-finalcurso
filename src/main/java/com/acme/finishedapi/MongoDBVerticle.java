package com.acme.finishedapi;

import com.acme.finishedapi.database.MongoManager;

import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.MongoClient;

public class MongoDBVerticle extends AbstractVerticle {
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoDBVerticle.class);

	private static MongoClient mongoClient =  null;
	
	public static void main(String[] args) {		
		VertxOptions vertxOptions = new VertxOptions();
		vertxOptions.setClustered(true);
		
		// Config. Cluster
		Vertx.clusteredVertx(vertxOptions, resultHandler -> {
			if (resultHandler.succeeded()) {
				Vertx vertx = resultHandler.result();
				
				// Read config.json
				ConfigRetriever configRetriever = ConfigRetriever.create(vertx);
				configRetriever.getConfig(config -> {
					if (config.succeeded()) {						
						JsonObject configJson = config.result();						
						DeploymentOptions options = new DeploymentOptions().setConfig(configJson);
						
						LOGGER.info("config.json: " + Json.encodePrettily(configJson));
						
						vertx.deployVerticle(new MongoDBVerticle(), options);
					}
				});
			}
		});
	}
	
	@Override
	public void start() throws Exception {
		LOGGER.info("Verticle MongoDBVerticle Started");
		
		final String connectionString = "mongodb://"
				+ config().getString("mongodb.host")
				+ ":" + config().getInteger("mongodb.port")
				+ "/" + config().getString("mongodb.databasename");
		
		JsonObject dbConfig = new JsonObject()
				.put("connection_string", connectionString)
//				.put("username", config().getString("mongodb.username"))
//				.put("password", config().getString("mongodb.password"))
//				.put("authSource", config().getString("mongodb.authSource"));
				.put("useObjectId", true);		
		
		mongoClient = MongoClient.createShared(vertx, dbConfig);
		
		MongoManager mongoManager = new MongoManager(mongoClient);
		mongoManager.registerConsumer(vertx);		
	}
	
	@Override
	public void stop() throws Exception {
		LOGGER.info("Verticle MongoDBVerticle Stopped");
	}
}
