package com.acme.finishedapi.database;

import java.util.List;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

public class MongoManager {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoManager.class);
	
	private MongoClient mongoClient;
	
	public MongoManager(final MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}
	
	public void registerConsumer(Vertx vertx) {
		vertx.eventBus().consumer("com.acme.mongoservice", message -> {	
			LOGGER.info("Received message: " + message.body());
			
			JsonObject jsonMessage = new JsonObject(message.body().toString());
			
			if ("findAll".equals(jsonMessage.getString("cmd"))) {
				getAllProducts(message);
			}
			
			if ("findById".equals(jsonMessage.getString("cmd"))) {
				getProductById(message, jsonMessage.getString("productId"));
			}
		});
	}
	
	/*
	 * Get all products as array of product from MongoDB
	 */
	public void getAllProducts(Message<Object> message) {		
		FindOptions findOptions = new FindOptions();
		
		// new JsonObject().put("number", "123")
		mongoClient.findWithOptions("products", new JsonObject(), findOptions, results -> {
			try {
				List<JsonObject> objects = results.result();
				if (objects != null && !objects.isEmpty()) {
					LOGGER.info("Got some data length = " + objects.size());				
					message.reply(new JsonObject().put("products", objects));
				} else {
					message.reply(new JsonObject().put("error", "No items found"));
				}
			} catch (Exception e) {
				LOGGER.error("getAllProducts Failed", e);				
				message.reply(new JsonObject().put("error", "Exceptions and No items found"));
			}
		});
	}
	
	public void getProductById(Message<Object> message, final String productId) {
		FindOptions findOptions = new FindOptions();		
		JsonObject jsonSearched = new JsonObject().put("_id", productId);
		
		mongoClient.findWithOptions("products", jsonSearched, findOptions, results -> {
			try {
				List<JsonObject> objects = results.result();
				if (objects != null && !objects.isEmpty()) {
					LOGGER.info("Got some data length = " + objects.size());				
					message.reply(objects.get(0).toString());
				} else {
					message.reply(new JsonObject().put("error", "No items found"));
				}
			} catch (Exception e) {
				LOGGER.error("getProductById Failed", e);				
				message.reply(new JsonObject().put("error", "Exceptions and No items found"));
			}
		});
	}
}
