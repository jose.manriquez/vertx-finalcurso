package com.acme.finishedapi.resources;

import com.acme.finishedapi.entity.Product;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ProductResources {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductResources.class);
	
	private Vertx vertx;
	
	public Router getAPISubRouter(final Vertx vertx) {
		this.vertx = vertx;
		
		Router apiSubRouter = Router.router(vertx);
		
		// API routing
		apiSubRouter.route("/*").handler(this::defaultProcessorForAllAPI);

		apiSubRouter.route("/v1/products*").handler(BodyHandler.create());
		apiSubRouter.get("/v1/products").handler(this::getAllProducts);
		apiSubRouter.get("/v1/products/:id").handler(this::getProductById);
		apiSubRouter.post("/v1/products").handler(this::addProduct);
		apiSubRouter.put("/v1/products/:id").handler(this::updateProductById);
		apiSubRouter.delete("/v1/products/:id").handler(this::deleteProductById);
		
		return apiSubRouter;
	}
	
	/*
	 * Get all products as array of product.
	 */
	public void getAllProducts(RoutingContext routingContext) {	
		JsonObject cmdJson = new JsonObject().put("cmd", "findAll");
		
		vertx.eventBus().send("com.acme.mongoservice", cmdJson, replyHandler -> {
			
			if (replyHandler.succeeded()) {
				JsonObject responseJson = new JsonObject(replyHandler.result().body().toString());				
			
				routingContext.response()
						.setStatusCode(200)
						.putHeader("Content-Type", "application/json")
						.end(Json.encodePrettily(responseJson));
			}	
		});
	}
	
	/*
	 * Get one product that matches the input id and return as single json object.
	 */
	public void getProductById(RoutingContext routingContext) {
		final String productId = routingContext.request().getParam("id");
		
		JsonObject cmdJson = new JsonObject()
				.put("cmd", "findById")
				.put("productId", productId);
		
		vertx.eventBus().send("com.acme.mongoservice", cmdJson, replyHandler -> {
			
			if (replyHandler.succeeded()) {
				JsonObject responseJson = new JsonObject(replyHandler.result().body().toString());				
			
				routingContext.response()
						.setStatusCode(200)
						.putHeader("Content-Type", "application/json")
						.end(Json.encodePrettily(responseJson));
			}	
		});
	}
	
	/*
	 * Insert one item passed in from the http post body.
	 * Return what was added with unique id from the insert.
	 */
	public void addProduct(RoutingContext routingContext) {
		JsonObject jsonBody = routingContext.getBodyAsJson();
		
		final String number = jsonBody.getString("number");
		final String description = jsonBody.getString("description");
		
		// Add into database and get unique id
		Product newItem = new Product("556677", number, description);
		
		routingContext.response().setStatusCode(201)
				.putHeader("Content-Type", "application/json")
				.end(Json.encodePrettily(newItem));
	}
	
	/*
	 * Update the item based on the url product id and return update product. 
	 */
	public void updateProductById(RoutingContext routingContext) {
		final String productId = routingContext.request().getParam("id");
		
		JsonObject jsonBody = routingContext.getBodyAsJson();		
		final String number = jsonBody.getString("number");
		final String description = jsonBody.getString("description");
		
		Product updatedItem = new Product(productId, number, description);
		
		routingContext.response().setStatusCode(200)
				.putHeader("Content-Type", "application/json")
				.end(Json.encodePrettily(updatedItem));
	}
	
	/*
	 * Delete item and return 200 on success or 400 on fail.
	 */
	public void deleteProductById(RoutingContext routingContext) {
		final String productId = routingContext.request().getParam("id");
		
		routingContext.response().setStatusCode(200)
				.putHeader("Content-Type", "application/json")
				.end();
	}
	
	public void defaultProcessorForAllAPI(RoutingContext routingContext) {
		final String authToken = routingContext.request().headers().get("AuthToken");
		
		if (!"123".equals(authToken)) {
			LOGGER.info("failed basic auth check");
			
			routingContext.response().setStatusCode(401)
					.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
					.end(new JsonObject().put("error", "Not Authotized to use these API's").toString());
		} else {
			LOGGER.info("Passed basic auth check");
			
			// Allowing CORS - Cross Domain API calls
			routingContext.response().putHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
			routingContext.response().putHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "GET,POST,PUT,DELETE");
			
			// Call next matching route
			routingContext.next();
		}
	}
}
